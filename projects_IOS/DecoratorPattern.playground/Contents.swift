protocol Coffee {
    func getCost() -> Int
    func getIngredients() -> String
}

class SimpleCoffee: Coffee {
    func getCost() -> Int {
        return 60
    }
    
    func getIngredients() -> String {
        return "Coffee"
    }
}

class CoffeeDecorator: Coffee {
    private let decoratedCoffee: Coffee
    private let ingerientSeparator: String = ", "
    
    required init(decoratedCoffee: Coffee) {
        self.decoratedCoffee = decoratedCoffee
    }
    
    func getCost() -> Int {
        return decoratedCoffee.getCost()
    }
    
    func getIngredients() -> String {
        return decoratedCoffee.getIngredients()
    }
}


class Milk:CoffeeDecorator {
    
    required init(decoratedCoffee: Coffee) {
        super.init(decoratedCoffee: decoratedCoffee)
    }
    
    override func getCost() -> Int {
        return decoratedCoffee.getCost() + 20
    }
    
    override func getIngredients() -> String {
        return decoratedCoffee.getIngredients() + ingerientSeparator + "Milk"
    }
}

class WhipCoffee: CoffeeDecorator {
    required init(decoratedCoffee: Coffee) {
        super.init(decoratedCoffee: decoratedCoffee)
    }
    
    override func getCost() -> Int {
        return decoratedCoffee.getCost() + 25
    }
    
    override func getIngredients() -> String {
        return decoratedCoffee.getIngredients() + ingerientSeparator + "Whip"
    }
}


var someCoffee = SimpleCoffee()
print("Costo de bebida : \(someCoffee.getCost()), e ingredientes: \(someCoffee.getIngredients())")
let milkCoffee = Milk(decoratedCoffee: someCoffee)
print("Costo de bebida : \(milkCoffee.getCost()), e ingredientes: \(milkCoffee.getIngredients())")
let premiumCoffee = WhipCoffee(decoratedCoffee: milkCoffee)
print("Costo de bebida : \(premiumCoffee.getCost()), e ingredientes: \(premiumCoffee.getIngredients())")