import UIKit
protocol Producto: CustomStringConvertible {
  var nombre: String { get }
  var tamanio_pantalla: Double { get }
  var precio: Double { get }
}

extension Producto {
  var desc: String {
    return "Un \(nombre) de \(tamanio_pantalla)\"pulgadas cuesta \(precio)"
  }
}
class IPhone: Producto {
  let nombre: String         = "iPhone 6"
  let tamanio_pantalla: Double = 4.7
  let precio: Double        = 799
}

class Macbook: Producto {
  let nombre: String         = "Macbook"
  let tamanio_pantalla: Double = 12
  let precio: Double        = 1449
}

class IMac: Producto {
  let nombre: String         = "iMac"
  let tamanio_pantalla: Double = 21.5
  let precio: Double        = 1249
}
enum ProducApple {
  case IPHONE
  case MACBOOK
  case IMAC
}
class Fabrica {
  func crea(product: ProducApple) -> Producto {
    switch product {
    case .IPHONE:
      return IPhone()
    case .MACBOOK:
      return Macbook()
    case .IMAC:
      return IMac()
    }
  }
}
let fabrica = Fabrica()

let iphone = fabrica.crea(.IPHONE)
print(iphone) 

let macbook = fabrica.crea(.MACBOOK)
print(macbook)

let imac = fabrica.crea(.IMAC)
print(imac) 