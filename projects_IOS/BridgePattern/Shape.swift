import Foundation
//clase de abstraccion

public class Shape {
    
    let graphicsApi: GraphicsAPI
    
    init(_ graphicsApi: GraphicsAPI) {
        self.graphicsApi = graphicsApi
    }
    
    func draw() -> Void {
        
    }
}
