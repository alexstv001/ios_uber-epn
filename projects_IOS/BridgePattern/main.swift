import Foundation

var openGLApi: OpenGLAPI = OpenGLAPI()
var directXApi: DirectXAPI = DirectXAPI()

var circle: Circle = Circle(10, 10, 10, openGLApi)
circle.draw()

circle = Circle(10, 5, 4, directXApi)
circle.draw()

var rect: Rectangle = Rectangle(14, 14, 14, 14, openGLApi)
rect.draw()

rect = Rectangle(12, 12, 12, 12, directXApi)
rect.draw()