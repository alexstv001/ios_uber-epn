import UIKit

class Vehiculo {
  let modelo: String
  let anio: UInt
  let numPuertas: UInt
  let color: UIColor
}

class VehiculoBuilder {

  // Bloque que toma como parametro a VehiculoBuilder
  typealias CierreBuilder = (VehiculoBuilder) -> ()

  // Variables que se necesitan para un vehiculo
  var modelo: String      = "Mazda 4"
  var anio: UInt         = 2016
  var numPuertas: UInt = 3
  var color: UIColor    = .whiteColor()

  // Constructor vacio que usa las variables predeterminadas
  init() {}

  // Toma en el parametro un CierreBuilder que se llama directamente
  init(CerrarBuild: CierreBuilder) {
    CerrarBuild(self)
  }
}

init(builder: VehiculoBuilder) {
  self.modelo        = builder.modelo
  self.anio         = builder.anio
  self.numPuertas = builder.numPuertas
  self.color       = builder.color
}

let chevrolet = VehiculoBuilder { builder in
  builder.modelo        = "Chevrolet"
  builder.anio         = 2012
  builder.numPuertas = 5
  builder.color       = .redColor()
}

let Vehiculo = NewVehiculo (builder: chevrolet)